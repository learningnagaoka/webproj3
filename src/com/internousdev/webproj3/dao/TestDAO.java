package com.internousdev.webproj3.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.internousdev.webproj3.util.DBConnector;

public class TestDAO {

	private String username;
	private String password;

	public int insert(String username, String password) {
		int ret = 0;

		DBConnector db = new DBConnector();
		Connection con = db.getConnection();

		//auto_incrementなどあるときは、insertするものを明示的に書かないといけない?
		String sql = "INSERT INTO users(user_name, password) VALUES(?, ?)";

		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);

			int i= ps.executeUpdate();
			if(i > 0) {
				ret = i;
				System.out.println(i + "件登録されました");
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		try {
			con.close();
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
