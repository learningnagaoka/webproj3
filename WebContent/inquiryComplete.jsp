<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>問い合せ完了</title>
</head>
<body>
<s:property value="name"/>さん、問い合せありがとうございました。
<br>
問い合わせ種類:
<s:if test='qtype=="company"'>会社について</s:if>
<s:if test='qtype=="product"'>製品について</s:if>
<s:if test='qtype=="support"'>アフターサポートについて</s:if>
<br>
問い合わせ内容:<s:property value="body"/>
</body>
</html>