drop database if exists testdb;

create database testdb;
use testdb;

create table users(
user_id int(11) primary key auto_increment,
user_name varchar(255),
password varchar(255)
);

insert into users values(1, "tanaka", "tanaka");
insert into users values(2, "ootani", "ootani");
insert into users values(3, "sasaki", "sasaki");

create table inquiry(
name varchar(255),
qtype varchar(255),
body varchar(255)
);